import React from 'react';
import ReactDOM from 'react-dom';
import HomeSider from './index';

it('Should render HomeSider component', () => {
  const div = document.createElement('div');
  ReactDOM.render(<HomeSider />, div);
  ReactDOM.unmountComponentAtNode(div);
});
