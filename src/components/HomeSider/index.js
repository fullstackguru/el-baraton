import React, { Component } from 'react'
import { Icon, Layout, Menu } from 'antd';
import logo from '../../assets/logo.svg';
import './styles/HomeSider.css';
import {Link} from "react-router-dom";

const { Sider } = Layout;

class HomeSider extends Component {
  constructor(){
    super();
    this.state = {
      collapsed: false
    };

    this.onCollapse = this.onCollapse.bind(this);
  }

  onCollapse = (collapsed) => {
    this.setState({
      collapsed : collapsed
    });
  };

  render(){
    return(
      <Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}>
        <img src={logo} className="animated-logo" alt="logo" />
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
          <Menu.Item key="1">
            <Link to='/categories'>
              <Icon type="appstore" />
              <span>Categorias</span>
            </Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to='/shopping-cart'>
              <Icon type="shopping-cart" />
              <span>Carro de Compras</span>
            </Link>
          </Menu.Item>
        </Menu>
      </Sider>
    )
  }
}

export default HomeSider