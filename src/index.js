import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, combineReducers, createStore} from "redux";
import {routerMiddleware, routerReducer} from 'react-router-redux';
import { Router, Route } from 'react-router';
import { Provider } from 'react-redux';
import 'assets/styles/ElBaraton.css';
import registerServiceWorker from './registerServiceWorker';
import {homeReducer} from "./scenes/Home/reducer";
import {createBrowserHistory} from "history";
import HomeContainer from "./scenes/Home/container";

const history = createBrowserHistory();

const reduxRouterMiddleware = routerMiddleware(history);

const combinedReducers = combineReducers({
  routerReducer,
  homeReducer
});

let reduxStore = createStore(
  combinedReducers,
  applyMiddleware(
    reduxRouterMiddleware
  )
);

ReactDOM.render(
  <Provider store={reduxStore}>
    <Router history={history}>
      <Route path="/" component={HomeContainer}/>
    </Router>
  </Provider>
  , document.getElementById('root')
);
registerServiceWorker();
