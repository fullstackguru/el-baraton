import { connect } from 'react-redux';
import Home from "../index";
import {receiveCategories} from "../actions";


const mapStateToProps = (state) => {
  return {

  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    receiveCategories: categories => dispatch(receiveCategories(categories))
  };
};

const HomeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

export default HomeContainer;