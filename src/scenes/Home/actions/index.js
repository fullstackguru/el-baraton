import * as types from './types';

export const receiveCategories = categories => ({
  type: types.CATEGORIES_RECEIVE,
  categories
});

export const selectCategory = category => ({
  type: types.CATEGORY_SELECT,
  category
});

export const toggleAvailableFilter = () => ({
  type: types.AVAILABLE_FILTER_TOGGLE
});

export const setPriceRangeFilter = (min, max) => ({
  type: types.PRICE_RANGE_FILTER_SET,
  min,
  max
});

export const setSubCategoryFilter = subCategoryId => ({
  type: types.SUB_CATEGORY_FILTER_SET,
  subCategoryId
});

export const receiveProducts = products => ({
  type: types.PRODUCTS_RECEIVE,
  products
});
