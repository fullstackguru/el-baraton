import { connect } from 'react-redux';
import Category from "../index";
import {selectCategory} from "../../../../../actions";

const mapStateToProps = (state) => {
  return {
    homeReducer : state.homeReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectCategory: category => dispatch(selectCategory(category))
  };
};

const CategoryContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Category);

export default CategoryContainer;