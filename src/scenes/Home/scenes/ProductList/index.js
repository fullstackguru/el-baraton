import React, { Component } from 'react';
import {Breadcrumb, Button, Card, Checkbox, Col, Icon, Layout, Row, Slider, TreeSelect} from "antd";
import {Link, withRouter} from "react-router-dom";
import dataProducts from 'data/products';
import dataCategories from 'data/categories';
import './styles/ProductList.css';

const { Content } = Layout;

class ProductList extends Component{
  constructor(){
    super();

    this.state = {
      filteredProducts: null,
      priceRange: {
        min: null,
        max: null
      },
      categoryTree: null
    };

    this.onAvailableChange = this.onAvailableChange.bind(this);
    this.onAfterChange = this.onAfterChange.bind(this);
    this.onSelectCategory = this.onSelectCategory.bind(this);
  }

  productInCategory(product, sublevel){
    let i = 0;
    let found = false;

    while (i < sublevel.length && !found){
      if (product.sublevel_id === sublevel[i].id)
        found = true;
      else {
        if (sublevel[i].sublevels)
          found = this.productInCategory(product, sublevel[i].sublevels);
        i++
      }
    }
    return found
  }

  /**
   * Fetch category from json file, and the match param id
   */
  fetchCategory(){
    const { categoryId } = this.props.match.params;
    const category = dataCategories.find(item => {
      return item.id === parseInt(categoryId, 10)
    });
    if (category){
      this.props.selectCategory(category);
      this.fetchProducts(category);
      this.generateTreeData(category);
    } else {
      this.props.history.push('/categories');
    }
  }

  /**
   * Fetch products from json file, filter by sublevelId and
   * send data in to Redux Store
   */
  fetchProducts(category){
    let products = [];
    let min = 9999999, max = 0;
    for (let i = 0; i < dataProducts.length; i++){
      if (this.productInCategory(dataProducts[i], category.sublevels)){
        products.push(dataProducts[i]);
        let price = parseInt(dataProducts[i].price.substr(1).replace(',', '.'), 10);
        if (price < min)
          min = price;
        else if (price > max)
          max = price;
      }
    }
    this.props.receiveProducts(products);
    this.setState({
      priceRange: {
        min: min,
        max: max + 1
      }
    }, () => {
      this.setFilters(this.props.homeReducer.selectedFilters)
    });
  }

  onAvailableChange(event){
    this.props.toggleAvailableFilter();
  }

  onAfterChange(value){
    this.props.setPriceRangeFilter(value[0], value[1])
  }

  onSelectCategory(value){
    this.props.setSubCategoryFilter(parseInt(value,10));
  }

  setFilters(filters){
    let {products} = this.props.homeReducer;

    if (filters.available){
      products = products.filter(item => {
        return item.available === true;
      })
    }

    if (filters.priceRange.min && filters.priceRange.max){
      let min = filters.priceRange.min;
      let max = filters.priceRange.max;
      products = products.filter(item => {
        const price = parseInt(item.price.substr(1).replace(',', '.'), 10);
        return price >= min && price <= max
      })
    }

    if (filters.subCategoryId){
      // TODO: This must be filter on parent of subCategoryId
      products = products.filter(item => {
        return item.sublevel_id === filters.subCategoryId
      })
    }

    this.setState({
      filteredProducts: products
    })
  }

  generateTreeData(category){
    const tree = this._generateTreeData(category.sublevels, []);
    this.setState({
      categoryTree: tree
    })
  }
  _generateTreeData(sublevels, tree){
    let i = 0;
    while (i < sublevels.length){
      tree.push({
        label: sublevels[i].name,
        key: `${sublevels[i].name}-${sublevels[i].id}`,
        value: `${sublevels[i].id}`,
        children: sublevels[i].sublevels ? this._generateTreeData(sublevels[i].sublevels, []) : null
      });
      i++
    }
    return tree
  }

  componentWillReceiveProps(nextProps){
    const nextFilters = nextProps.homeReducer.selectedFilters;
    const currentFilters = this.props.homeReducer.selectedFilters;
    if (JSON.stringify(nextFilters) !== JSON.stringify(currentFilters)){
      this.setFilters(nextFilters)
    }
  }

  componentDidMount() {
    const category = this.props.homeReducer.selectedCategory;
    if (category) {
      this.fetchProducts(category);
      this.generateTreeData(category);
    } else
      this.fetchCategory();
  }

  render(){
    const {filteredProducts, priceRange} = this.state;
    const {available} = this.props.homeReducer.selectedFilters;
    const category = this.props.homeReducer.selectedCategory;

    if (!category){
      return(
        <div>Cargando...</div>
      )
    }

    return(
      <Content className="product-list">
        <Breadcrumb className="breadcrumb">
          <Breadcrumb.Item><Link to='/categories'>Categories</Link></Breadcrumb.Item>
          <Breadcrumb.Item><Link to={`/categories/${category.id}/products`}>{category.name}</Link></Breadcrumb.Item>
        </Breadcrumb>
        <div className="page-content">
          <Row type="flex" justify="center">
            <h2>Ve nuestros productos</h2>
          </Row>
          <Row className="filters" gutter={24}>
            <Col span={6} className="filter">
              <Checkbox checked={available} onChange={this.onAvailableChange}>¿Disponible?</Checkbox>
            </Col>
            <Col span={6} className="filter">
              Sub-categoria
              <TreeSelect
                className="tree-select"
                treeData={this.state.categoryTree}
                placeholder="Please select"
                treeDefaultExpandAll
                onChange={this.onSelectCategory}
              />
            </Col>
            <Col span={6} className="filter">
              Rango de precios
              <Slider
                range
                min={priceRange.min}
                max={priceRange.max}
                onAfterChange={this.onAfterChange}/>
            </Col>
            <Col span={6} className="filter">
            </Col>
          </Row>
          <Row type="flex" gutter={16}>
            {
              filteredProducts ? (
                filteredProducts.map(product => (
                  <Col
                    span={8}
                    key={`product${product.id}`}>
                    <Card
                      hoverable
                      title={product.name}
                      extra={product.available ? (<Icon type="unlock" />) : (<Icon type="lock" />)}
                      className="product">
                      <Row>
                        <Col span={12}>
                          Stock = {product.quantity} <br/>
                          Precio = {product.price}
                        </Col>
                        <Col span={12}>
                          <Button
                            type="primary"
                            icon="shopping-car"
                            disabled={!product.available}
                            size="large">
                            Add to cart
                          </Button>
                        </Col>
                      </Row>
                    </Card>
                  </Col>
                ))
              ) : (
                <h4>Cargando...</h4>
              )
            }
            <Col span={12}>

            </Col>
          </Row>
        </div>
      </Content>
    )
  }
}

export default withRouter(ProductList)