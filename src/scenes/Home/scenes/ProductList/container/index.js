import { connect } from 'react-redux';
import ProductList from "../index";
import {
  receiveProducts, selectCategory, setPriceRangeFilter, setSubCategoryFilter,
  toggleAvailableFilter
} from "../../../actions";

const mapStateToProps = (state) => {
  return {
    homeReducer : state.homeReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectCategory: category => dispatch(selectCategory(category)),
    toggleAvailableFilter: () => dispatch(toggleAvailableFilter()),
    setPriceRangeFilter: (min, max) => dispatch(setPriceRangeFilter(min, max)),
    setSubCategoryFilter: subCategoryId => dispatch(setSubCategoryFilter(subCategoryId)),
    receiveProducts: products => dispatch(receiveProducts(products))
  };
};

const ProductListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductList);

export default ProductListContainer;