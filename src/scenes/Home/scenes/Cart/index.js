import React, { Component } from 'react';
import {Breadcrumb, Layout} from "antd";

const { Content } = Layout;

class Cart extends Component{

  render(){
    return(
      <Content>
        <Breadcrumb className="breadcrumb">
          <Breadcrumb.Item>Cart</Breadcrumb.Item>
        </Breadcrumb>
        <div className="page-content">
          This is a Cart
        </div>
      </Content>
    )
  }
}

export default Cart